// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var http_pb = require('./http_pb.js');

function serialize_httpgrpc_Empty(arg) {
  if (!(arg instanceof http_pb.Empty)) {
    throw new Error('Expected argument of type httpgrpc.Empty');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_httpgrpc_Empty(buffer_arg) {
  return http_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_httpgrpc_Pong(arg) {
  if (!(arg instanceof http_pb.Pong)) {
    throw new Error('Expected argument of type httpgrpc.Pong');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_httpgrpc_Pong(buffer_arg) {
  return http_pb.Pong.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_httpgrpc_Request(arg) {
  if (!(arg instanceof http_pb.Request)) {
    throw new Error('Expected argument of type httpgrpc.Request');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_httpgrpc_Request(buffer_arg) {
  return http_pb.Request.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_httpgrpc_Response(arg) {
  if (!(arg instanceof http_pb.Response)) {
    throw new Error('Expected argument of type httpgrpc.Response');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_httpgrpc_Response(buffer_arg) {
  return http_pb.Response.deserializeBinary(new Uint8Array(buffer_arg));
}


var WorkerService = exports.WorkerService = {
  fetch: {
    path: '/httpgrpc.Worker/Fetch',
    requestStream: false,
    responseStream: false,
    requestType: http_pb.Request,
    responseType: http_pb.Response,
    requestSerialize: serialize_httpgrpc_Request,
    requestDeserialize: deserialize_httpgrpc_Request,
    responseSerialize: serialize_httpgrpc_Response,
    responseDeserialize: deserialize_httpgrpc_Response,
  },
  ping: {
    path: '/httpgrpc.Worker/Ping',
    requestStream: false,
    responseStream: false,
    requestType: http_pb.Empty,
    responseType: http_pb.Pong,
    requestSerialize: serialize_httpgrpc_Empty,
    requestDeserialize: deserialize_httpgrpc_Empty,
    responseSerialize: serialize_httpgrpc_Pong,
    responseDeserialize: deserialize_httpgrpc_Pong,
  },
};

exports.WorkerClient = grpc.makeGenericClientConstructor(WorkerService);
