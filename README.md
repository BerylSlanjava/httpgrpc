# httpgrpc

### Generate Go file
```
docker run --rm -v $(pwd):/dir -w "/dir" grpc/go protoc http.proto --go_out=plugins=grpc:.
```
### Generate Node files
```
docker run --rm -v $(pwd):/dir -w "/dir" grpc/node bash node-build.sh
```