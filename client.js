"use strict";

const { credentials } = require('grpc');
const httpStatus = require('http-status');
const { WorkerClient } = require('./http_grpc_pb');
const { Request, HeaderValues } = require('./http_pb');

class GoHTTPClient {
  constructor (address = "localhost:50051", creds = credentials.createInsecure(), options = {}) {
    this.client = new WorkerClient(address, creds, options);
    this.fetch = this.fetch.bind(this);
  }

  async fetch (url = "", { headers = {}, method = "GET", body = "" } = {}) {
    const resp = await new Promise((resolve, reject) => {
      const req = new Request();
      req.setMethod(method);
      req.setUrl(url);
      Object.entries(headers).reduce(
        (map, [key, value]) => map.set(key, value),
        req.getHeadersMap()
      );
      req.setBody(body);

      this.client.fetch(req, (err, res) => err ? reject(err) : resolve(res));
    })
    const status = resp.getStatus();
    return {
      url: resp.getUrl(),
      ok: status >= 200 && status <= 299,
      status: status,
      statusText: httpStatus[status],
      body: resp.getBody(),
      text: async () => this.body,
      headers: Object.assign(
        resp.getHeadersMap().toObject(false, HeaderValues.toObject).reduce((res, [ key, { valuesList }]) => {
          res[key] = valuesList.length === 1 ? valuesList[0] : valuesList
          return res
        }, {}),
        { forEach: cb => { Object.entries(this).filter(([k]) => k !== "forEach").forEach(([k, v], i, o) => cb(v, k, o)) } }
      )
    };
  }

  close () {
    this.client.close();
  }
}

module.exports = GoHTTPClient;

/*
void async function main() {
  const client = new GoHTTPClient();
  console.time("took");
  const resp = await client.fetch("https://esi.tech.ccp.is/latest/status/?datasource=tranquility");
  console.timeEnd("took");
  client.close();
}();
*/
