#!/bin/bash
npm i
node_modules/.bin/grpc_tools_node_protoc --js_out=import_style=commonjs,binary:. --grpc_out=. --plugin=protoc-gen-grpc=./node_modules/.bin/grpc_tools_node_protoc_plugin  --ts_out=service=true:. --plugin=protoc-gen-ts=./node_modules/.bin/protoc-gen-ts http.proto
rm http_pb_service.ts
rm -rf node_modules